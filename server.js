// const express = require('express');
// const app = express();

// app.get('/', function (req, res) {
//     res.send('Hello World!')
// })

// app.post('/', function (req, res) {
//     res.send('Got a POST request')
// })

// app.put('/api/user/:id', function (req, res) {
//     res.send('Got a PUT request at /user')
// })

// app.delete('/api/user/:id', function (req, res) {
//     res.send('Got a DELETE request at /user')
// })

// app.get('/auth/google',
//   passport.authenticate('google', { scope: 
//   	[ 'https://www.googleapis.com/auth/plus.login',
//   	, 'https://www.googleapis.com/auth/plus.profile.emails.read' ] }
// ));
 
// app.get( '/auth/google/callback', 
//     passport.authenticate( 'google', { 
//         successRedirect: '/auth/google/success',
//         failureRedirect: '/auth/google/failure'
// }));

// app.listen(8080, () => console.log('listening on port 3000!'))


const express = require("express");
const app = express();
const path = require("path");
const mongoose = require("mongoose");
const routineSchema = require("./src/backend/routineSchema");
const port = 8080;
const bodyParser = require("body-parser");

mongoose.connect("mongodb://localhost:27017/exercise_routine", { useNewUrlParser: true });
mongoose.Promise = Promise;
var db = mongoose.connection;

app.use(express.static(__dirname + "/build"));
// app.use(express.static("build/static"));
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.get('/api/routines', (req, resp) => {
  db.collection('routine').find({}).toArray((error, posts) => {
    resp.json({ posts });
  });
});

app.use(bodyParser.json());


app.get('*', function (req, res) {
  res.sendfile(__dirname + "/build/index.html");
});

app.listen(port, () => console.log(`server is running on localhost ${port}`));
