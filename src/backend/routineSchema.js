var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var routineSchema = new Schema({
  
 routineId : String,
 routineName : String,
 routineDescription : String,
 routineExercises : [{
  
    exerciseId : String,
    exerciseName : String,
    duration : Number,
    rest : Number,
    description : String,
    steps : String,

 }],

 routineDuration : Number,
 routineRest : Number

});

var Routine = mongoose.model('Routine', routineSchema);


