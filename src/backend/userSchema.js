var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
  
    id : String,
    name : String,
    userName : String,
    email : String,
    password : String,
    mobile : String,
    gender : String,
    age : Number

});

var User = mongoose.model('User', userSchema);


