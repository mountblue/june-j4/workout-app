import React from 'react'
import abdominalCrunches from '../images/warmup/abdominal-crunches.jpg'
import neckRotation from '../images/warmup/neck-rotation.jpg'
import highKneeTaps from '../images/warmup/high-knee-taps.jpg'
import jumpingJacks from '../images/warmup/Jumping-Jacks.jpg'
import legAbsWorkout from '../images/warmup/leg-raise-ab-workout.jpg'
import pushups from '../images/warmup/pushups.png'
import squat from '../images/warmup/squat.jpg'
import tricepDipsChair from '../images/warmup/tricep-dips-onchair.jpg'
import plank from '../images/warmup/plank.jpeg'
import superman from '../images/warmup/superman.jpg'
import lunge from '../images/warmup/lunge.jpeg'
import donkeyKicks from '../images/warmup/donkey-kicks-butt-workout.png'

export default class ExerciseRoutine extends React.Component {
    render() {
        return (
            <div>
                <div className="warmup">
                    <div>
                        <img className="warmup-image" src={neckRotation} />
                    </div>
                    <div>
                        <p>
                            5-10 rolls each direction
                            1.Begin with your head straight and looking forward.
                            2.Gently tilt your head to right and start rolling it back.
                            3.keep rolling your head to the left and then down.
                            4.Bring your head up to the straight position and repeat in the opposite direction.
                        </p>
                    </div>
                </div>
                <hr />
                <div className="warmup">
                    <div>
                        <img className="warmup-image" src={highKneeTaps} />
                    </div>
                    <div>
                        <p>
                            1. Stand straight with the feet hip width apart, looking straight ahead and arms hanging down by your side
                            2. Jump from one foot to the other at the same time lifting your knees as high as possible, hip height is advisable
                            3. The arms should be following the motion
                        </p>
                    </div>
                </div>
                <hr />
                <div className="warmup">
                    <div>
                        <img className="warmup-image" src={lunge} />
                    </div>
                    <div>
                        <p>
                            1. Place the hands on the hips, pull the shoulders back and stand tall.
                            2. Step forwards with your right leg and slowly lower the body until the front knee is bent to 90 degrees.
                            3. The back knee should never touch the floor.
                            4. Push yourself back up to the starting position as quickly but safely as possible.
                            5. Repeat with the left leg.
                    </p>
                    </div>
                </div>
                <hr />
                <div className="warmup">
                    <div>
                        <img className="warmup-image" src={squat} />
                    </div>
                    <div>
                        <p>
                            1. Stand tall with your feet hip width apart and your arms down by your side
                            2.Start to lower your body back as far as you can by pushing your hips back and bending your knees and pushing your body weight into your heels
                            3.As you are lowering into the squat your arms will start to raise out in front of you for balance
                            4.Keep a neutral spine at all times and never let your knees go over your toes
                            5.The lower body should be parallel with the floor and your chest should be lifted at all times not rounded.
                            6.Pause then lift back up in a controlled movement to the starting position
                    </p>
                    </div>
                </div>
                <hr />
                <div className="warmup">
                    <div>
                        <img className="warmup-image" src={tricepDipsChair} />
                    </div>
                    <div>
                        <p>
                            1. Hoist yourself up onto a bench, chair or step as long as it is stable and secure to take your body weight.
                            2.Hands should be shoulder width apart fingers facing forward and elbows pointing backwards with a slight bend in the elbows.
                            3.Legs extended out in front of you with a slight bend in the knee.
                            4.Slowly lower your body until your shoulder joints are below your elbows.
                            5.Push back up until your elbows are nearly straight but do not lock them out.
                    </p>
                    </div>
                </div>
                <hr />
                <div className="warmup">
                    <div>
                        <img className="warmup-image" src={abdominalCrunches} />
                    </div>
                    <div>
                        <p>
                            1. Lie flat on the floor – you can use a mat if you prefer.
                            2. Bend your knees.
                            3. Lets your arms stay crossed in the front of your chest.
                            4. Lift your head, making sure you are facing the ceiling. Use your abdominal muscles for the pull and then hold.
                            5. Ease back as you inhale. Do not push yourself down and control the movement.
                    </p>
                    </div>
                </div>
                <hr />



                <div className="warmup">
                    <div>
                        <img className="warmup-image" src={plank} />
                    </div>
                    <div>
                        <p>
                            1. Start by getting into a press up position on the ground.
                            2.Bend your elbows and rest your weight on your forearms and not on your hands.
                            3.Your body should form a straight line from shoulders to ankles.
                            4.Engage your core by sucking your belly button into your spine.
                            5.Hold this position for the prescribed time.
                    </p>
                    </div>
                </div>
                <hr />
                <div className="warmup">
                    <div>
                        <img className="warmup-image" src={superman} />
                    </div>
                    <div>
                        <p>
                            1.Lie face down on your stomach with the arms extended out in front of you and the legs extended behind you
                            2.In one movement lift the arms and legs up towards the ceiling making a U shape
                            3.Make sure that you do not lock out the limbs and keep the core as still as possible
                            4.Hold for 2-5 seconds and lower back down to complete.
                    </p>
                    </div>
                    <hr />
                </div>
                {/* <div className="warmup">
                <div>
                    <img className="warmup-image" src={jumpingJacks} />
                </di</div>
                <div>
                v>
                <hr />
                <div className="warmup">
                <div>
                    <img className="warmup-image" src={legAbsWorkout} />
                </di</div>
                <div>
                v>
                </div>
                <hr /> */}
                <div className="warmup">
                    <div>
                        <img className="warmup-image" src={pushups} />
                    </div>
                    <div>
                        <p>
                            1.Start on all fours with the hands on the floor slightly wider than but in line with the shoulders
                            2.The body should form a straight line from the shoulders to the ankles
                            3.Squeeze the abs as tight as possible and keep them engaged
                            4.Lower the body until the chest nearly touches the floor, making sure that the elbows are tucked in close to the torso
                            5.Pause for a moment then push yourself back to the starting position
                    </p>
                    </div>
                </div>
                <hr />
                <div className="warmup">
                    <div>
                        <img className="warmup-image" src={donkeyKicks} />
                    </div>
                    <div>

                        <p>
                            1.Get on all fours so that your hands are shoulder width apart and your knees are straight below your hips.
                            2.Bracing your abdominals and keeping your knee bent lift one leg up behind you until it is in line with your body and your foot is parallel to the ceiling.
                            3.Lower back down to the starting position and repeat with the other leg.
                        </p>

                    </div>

                </div>
                <hr />
            </div>

        )
    }
}