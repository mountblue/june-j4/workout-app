import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import Registration from './Registration';
import banner from '../images/banner.jpg'

class Home extends Component {
  googlAuth = () => {
    console.log("ok")
  }
  render() {
    return (
      <div className="container">
        <img src={banner} />
        <div className="get-started">
        <Link to='/registration'><button>Get Started</button></Link>
      </div>
      </div>
    )
  }
}

export default Home;