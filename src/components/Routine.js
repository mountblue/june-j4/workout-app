import React, { Component } from 'react'
import YogaBanner from '../images/yoga/yoga.jpg';
import PilateBanner from '../images/pilates/pilates.jpg';
import WarmupBanner from '../images/pilates/pilates.jpg';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class Routine extends Component {
  render() {
    return (
      <div className="routine-container">
        <div className="routine-content">
          <h1>Yoga</h1>
          <Link to="/yoga"><img src={YogaBanner} /></Link>
        </div>
        <div className="routine-content">
          <h1>Pilates</h1>
          <Link to="/pilates"><img src={PilateBanner} /></Link>
        </div>
        <div className="routine-content">
          <h1>Pilates</h1>
          <Link to="/warmup"><img src={WarmupBanner} /></Link>
        </div>
      </div>
    )
  }
}

export default Routine;