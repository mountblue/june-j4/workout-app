import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Header extends Component {
    render() {
        return (
            <header>
                <div className="logo"><h1>LOGO</h1></div>
                <div className="navigation-bar">
                    <Link className="link" to="/">Home</Link>
                    <Link className="link" to="/routines">Routine</Link>
                    <Link className="link" to="/exercise">Exercise</Link>
                    <Link className="link" to="/about">About</Link>
                    <Link className="link" to="/contact">Contact</Link>
                </div>
            </header>
        )
    }
}

export default Header;