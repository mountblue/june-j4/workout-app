import React, { Component } from 'react'
import Header from './Header';
import Body from './Body';
import Footer from './Footer';

class Main extends Component {
  render() {
    return (
      <div className="main-container">
        <Header />
      </div>
    )
  }
}

export default Main;