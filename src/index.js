import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Provider } from "react-redux"

import Registration from "./components/Registration"
import Login from "./components/Login"
import SignUp from "./components/Signup"
import Exercise from "./components/Exercise";
import Yoga from "./components/Yoga";
import Home from './components/Home';
import Main from './components/view/Main';
import Routine from './components/Routine';
import Pilates from './components/Pilates';
import Warmup from './components/Warmup';

ReactDOM.render(
    <Provider>
        <BrowserRouter>
            <div>
                <Main />
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/routines" component={Routine} />
                    <Route path='/registration' component={Registration} />
                    <Route path='/login' component={Login} />
                    <Route path='/signup' component={SignUp} />
                    <Route exact path="/yoga" component={Yoga} />
                    <Route exact path="/pilates" component={Pilates} />
                    <Route exact path="/warmup" component={Warmup} />
                </Switch>
            </div>
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
