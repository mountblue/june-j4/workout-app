import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Exercise from '../components/Exercise'
import Yoga from '../components/Yoga'

const ExerciseRoute = () => (
  <Router>
    <div>
      <ul>
        <li>
          <Link to="/Exercises">Exercise</Link>
        </li>
      </ul>
      <hr />
      
      {/* <Route exact path="/" component={Home} /> */}
      <Route path="/Exercise" component={Exercise} />
      <Route path="/Yoga" component={Yoga} />
    </div>
   </Router>
);
export default ExerciseRoute;